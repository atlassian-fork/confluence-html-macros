package com.atlassian.confluence.plugins.macros.html;

import com.atlassian.applinks.api.ReadOnlyApplicationLink;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.sal.api.net.ResponseHandler;
import org.apache.http.HttpHeaders;
import org.junit.Test;
import org.mockito.stubbing.Answer;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class HtmlIncludeMacroTestCase extends WhitelistedHttpRetrievalMacroTestCase<HtmlIncludeMacro> {
    protected HtmlIncludeMacro createMacro() {
        return new HtmlIncludeMacro(
                localeManager,
                i18NBeanFactory,
                requestFactory,
                applicationLinkService,
                whitelist
        );
    }

    @Test
    public void testOnlyBodyOfSuccessfulResponseRendered() throws IOException, ResponseException {
        final String successfulResponseHtml = getClasspathResourceAsString("com/atlassian/confluence/plugins/macros/html/successful-response.html");

        when(response.getHeader(HttpHeaders.CONTENT_TYPE)).thenReturn("text/html");
        when(response.getResponseBodyAsString()).thenReturn(successfulResponseHtml);

        assertEquals("This is my body",
                macro.successfulResponse(macroParameters, null, "http://localhost:8080/confluence", response));
    }

    @Test
    public void testNotPermitted() throws IOException, ResponseException {
        final String successfulResponseHtml = getClasspathResourceAsString("com/atlassian/confluence/plugins/macros/html/successful-response.html");

        when(response.getHeader(HttpHeaders.CONTENT_TYPE)).thenReturn("text/html");
        when(response.getResponseBodyAsString()).thenReturn(successfulResponseHtml);

        assertEquals("This is my body",
                macro.successfulResponse(macroParameters, null, "http://localhost:8080/confluence", response));
    }

    @Test
    public void testNotFound() throws IOException, ResponseException {
        final String successfulResponseHtml = getClasspathResourceAsString("com/atlassian/confluence/plugins/macros/html/successful-response.html");

        when(response.getHeader(HttpHeaders.CONTENT_TYPE)).thenReturn("text/html");
        when(response.getResponseBodyAsString()).thenReturn(successfulResponseHtml);

        assertEquals("This is my body",
                macro.successfulResponse(macroParameters, null, "http://localhost:8080/confluence", response));
    }

    @Test
    public void testUnsupportedContentTypeErrorRenderedIfContentTypeHeaderDoesNotExists() {
        assertEquals("<span class=\"error\">htmlinclude.error.content.type.not.supported</span>",
                macro.successfulResponse(macroParameters, null, "http://localhost:8080/confluence", response));
    }

    @Test
    public void testUnsupportedContentTypeErrorRenderedIfContentTypeIsNotHtml() {
        when(response.getHeader(HttpHeaders.CONTENT_TYPE)).thenReturn("application/octet-stream");

        assertEquals("<span class=\"error\">htmlinclude.error.content.type.not.supported</span>",
                macro.successfulResponse(macroParameters, null, "http://localhost:8080/confluence", response));
    }

    @Test
    public void testResponseDecodedUsingDefaultHtmlEncodingIfCharacterSetSpecifiedWithContentTypeHeaderIsInvalid() throws IOException, ResponseException {
        final String successfulResponseHtml = getClasspathResourceAsString("com/atlassian/confluence/plugins/macros/html/successful-response.html");

        when(response.getHeader(HttpHeaders.CONTENT_TYPE)).thenReturn("text/html; charset=invalidCharacterSet");
        when(response.getResponseBodyAsString()).thenReturn(successfulResponseHtml);


        assertEquals("This is my body",
                macro.successfulResponse(macroParameters, null, "http://localhost:8080/confluence", response));
    }

    @Test
    public void testResponseDecodedUsingCharacterSetSpecifiedByContentTypeHeader() throws ResponseException {
        // Title text of www.atlassian.jp
        final String successfulResponseHtml = "\u30a2\u30c8\u30e9\u30b7\u30a2\u30f3 \u30b8\u30e3\u30d1\u30f3 - JIRA\uff1a\u753b\u671f\u7684\u306a\u8ab2\u984c\u30c8\u30e9\u30c3\u30ad\u30f3\u30b0! CONFLUENCE\uff1a\u30a8\u30f3\u30bf\u30fc\u30d7\u30e9\u30a4\u30ba \u30a6\u30a3\u30ad";

        when(response.getHeader(HttpHeaders.CONTENT_TYPE)).thenReturn("text/html; charset=shift_jis");
        when(response.getResponseBodyAsString()).thenReturn(successfulResponseHtml);

        assertEquals(successfulResponseHtml,
                macro.successfulResponse(macroParameters, null, "http://localhost:8080/confluence", response));
    }

    @Test
    public void testResponseDecodedUsingCharacterSetSpecifiedByContentTypeHeaderInMetaTag() throws ResponseException {
        // Title text of www.atlassian.jp
        final String expectedText =
                "\u30a2\u30c8\u30e9\u30b7\u30a2\u30f3 \u30b8\u30e3\u30d1\u30f3 - JIRA\uff1a\u753b\u671f\u7684\u306a\u8ab2\u984c\u30c8\u30e9\u30c3\u30ad\u30f3\u30b0! CONFLUENCE\uff1a\u30a8\u30f3\u30bf\u30fc\u30d7\u30e9\u30a4\u30ba \u30a6\u30a3\u30ad";
        final String successfulResponseHtml =
                "<html>" +
                        "<head>" +
                        "<title>Test Page</title>" +
                        "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=shift_jis\">" +
                        "</head>" +
                        "<body>" + expectedText + "</body>" +
                        "</html>";


        when(response.getHeader(HttpHeaders.CONTENT_TYPE)).thenReturn("text/html");
        when(response.getResponseBodyAsString()).thenReturn(successfulResponseHtml);

        assertEquals(expectedText,
                macro.successfulResponse(macroParameters, null, "http://localhost:8080/confluence", response));
    }

    @Test
    public void testErrorMessageRenderedWhenIoExceptionThrownWhileReadingResponse() throws ResponseException {
        when(response.getHeader(HttpHeaders.CONTENT_TYPE)).thenReturn("text/html");
        when(response.getResponseBodyAsString()).thenThrow(new ResponseException("Fake ResponseException"));

        assertEquals("<span class=\"error\">Fake ResponseException</span>",
                macro.successfulResponse(macroParameters, null, "http://localhost:8080/confluence", response));
    }

    @Test
    public void testGetResponseFromApplicationLinkRequestFactory() throws Exception {
        String url = "http://localhost:1990/browse/TST";
        final String successfulResponse = "successfulResponse";

        macroParameters.put("url", url);

        final List<ReadOnlyApplicationLink> applicationLinks = singletonList(new MockReadOnlyApplicationLink("http://localhost:1990", applicationLinkRequestFactory));

        when(applicationLinkService.getApplicationLinks()).thenReturn(new Iterable<ReadOnlyApplicationLink>() {
            @Override
            @Nonnull
            public Iterator<ReadOnlyApplicationLink> iterator() {
                return applicationLinks.iterator();
            }
        });

        when(response.getStatusCode()).thenReturn(204);
        when(response.getHeader(HttpHeaders.CONTENT_TYPE)).thenReturn("text/html");
        when(response.getResponseBodyAsString()).thenReturn(successfulResponse);

        when(requestFactory.createRequest(any(), any())).thenThrow(new RuntimeException("Not expected requestFactory.createRequest call"));
        when(applicationLinkRequestFactory.createRequest(any(), any())).thenReturn(applicationLinkRequest);

        doAnswer((Answer<Void>) invocation -> {
            ResponseHandler handler = (ResponseHandler) invocation.getArguments()[0];
            handler.handle(response);
            return null;
        }).when(applicationLinkRequest).execute(any(ResponseHandler.class));

        HtmlIncludeMacro htmlIncludeMacro = createMacro();

        assertEquals(successfulResponse, htmlIncludeMacro.execute(macroParameters, null, pageToBeRendered.toPageContext()));
        verify(requestFactory, times(0)).createRequest(any(), any());
        verify(applicationLinkRequest, times(1)).execute(any(ResponseHandler.class));
    }
}
