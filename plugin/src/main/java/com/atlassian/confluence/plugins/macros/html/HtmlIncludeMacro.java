package com.atlassian.confluence.plugins.macros.html;

import com.atlassian.applinks.api.ReadOnlyApplicationLinkService;
import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugins.whitelist.OutboundWhitelist;
import com.atlassian.sal.api.net.NonMarshallingRequestFactory;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;
import com.opensymphony.module.sitemesh.Page;
import com.opensymphony.module.sitemesh.parser.HTMLPageParser;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpHeaders;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Map;
import java.util.regex.Pattern;

import static java.util.Collections.singletonList;

/**
 * Replace the radeox html-include macro
 */
public class HtmlIncludeMacro extends WhitelistedHttpRetrievalMacro
{
    private static final Logger logger = LoggerFactory.getLogger(HtmlIncludeMacro.class);
    private static final Pattern HTML_TYPE_PATTERN = Pattern.compile("(?i)(content-type:\\s*?)?\\Qtext/html\\E.*$");

    @Autowired
    public HtmlIncludeMacro(
            @ComponentImport LocaleManager localeManager,
            @ComponentImport I18NBeanFactory i18NBeanFactory,
            @ComponentImport NonMarshallingRequestFactory<Request<?, Response>> requestFactory,
            @ComponentImport ReadOnlyApplicationLinkService applicationLinkService,
            @ComponentImport OutboundWhitelist whitelist) {
        super(localeManager, i18NBeanFactory, requestFactory, applicationLinkService, whitelist);
    }

    @Override
    protected String successfulResponse(final Map parameters, final ConversionContext renderContext, final String url, final Response response)
    {
        final String contentType = StringUtils.defaultString(StringUtils.trim(response.getHeader(HttpHeaders.CONTENT_TYPE)));

        if (!HTML_TYPE_PATTERN.matcher(contentType).matches()) {
            logger.debug("Content type is: {}", contentType);
            return errorContent(getText("htmlinclude.error.content.type.not.supported", singletonList(url)));
        }

        try {
            final Page siteMeshPage = new HTMLPageParser().parse(response.getResponseBodyAsString().toCharArray());

            try (Writer writer = new StringWriter()) {
                siteMeshPage.writeBody(writer);
                return writer.toString();
            }
        }
        catch (final IOException | ResponseException ioe) {
            logger.error("IOException occured while parsing: " + url, ioe);
            return errorContent(ioe.getMessage());
        }
    }

    private static String errorContent(String message) {
        return "<span class=\"error\">" + StringEscapeUtils.escapeHtml(message) + "</span>";
    }
}
