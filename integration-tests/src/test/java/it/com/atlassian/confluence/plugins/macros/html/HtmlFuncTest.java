package it.com.atlassian.confluence.plugins.macros.html;

import com.atlassian.confluence.plugin.functest.ConfluenceWebTester;

public class HtmlFuncTest extends AbstractConfluencePluginWebTestCaseBase {
    private static final String MACRO_NAME = "html";

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        enableHtmlMacro();
    }

    public void testHtmlWithHref() {
        final long testPageId = createPage("HtmlFuncTest.testHtmlWithHref",
                "{" + MACRO_NAME + "}" +
                        "<a href=\"http://www.atlassian.com\">click here</a>" +
                        "{" + MACRO_NAME + "}");

        viewPageById(testPageId);

        assertElementPresentByXPath("//div[@class='wiki-content']//a[text()='click here']");
    }

    private void enableHtmlMacro() {
        String pluginKey = "confluence.macros.html-key";
        ConfluenceWebTester confluenceWebTester = getConfluenceWebTester();
        confluenceWebTester.enablePlugin(pluginKey, true);
        confluenceWebTester.enablePluginModule(pluginKey, "html-key", true);
        confluenceWebTester.enablePluginModule(pluginKey, "html-xhtml-key", true);
        confluenceWebTester.enablePluginModule(pluginKey, "html-migration-key", true);
    }

    /**
     * Test to make sure macros don't get rendered when its inside html macro
     */
    public void testHtmlWithMacro() {
        final long testPageId = createPage("HtmlFuncTest.testHtmlWithMacro",
                "{" + MACRO_NAME + "}" +
                        "<strong>{info}Info Text{info}</strong>" +
                        "{" + MACRO_NAME + "}");

        viewPageById(testPageId);

        assertEquals("{info}Info Text{info}", getElementTextByXPath("//div[@class='wiki-content']//strong[1]"));
    }
}
