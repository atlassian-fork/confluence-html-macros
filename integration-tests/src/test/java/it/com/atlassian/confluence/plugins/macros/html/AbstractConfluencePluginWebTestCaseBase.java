package it.com.atlassian.confluence.plugins.macros.html;

import com.atlassian.confluence.plugin.functest.AbstractConfluencePluginWebTestCase;
import com.atlassian.confluence.plugin.functest.ConfluenceWebTester;
import com.atlassian.confluence.plugin.functest.JWebUnitConfluenceWebTester;
import com.atlassian.confluence.plugin.functest.TesterConfiguration;
import com.atlassian.confluence.plugin.functest.helper.PageHelper;
import com.atlassian.federation.backdoor.CtkServerResourceModifier;
import com.atlassian.plugins.whitelist.testing.WhitelistTestRule;
import junit.framework.AssertionFailedError;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Date;
import java.util.Properties;

import static java.util.Collections.emptyList;

public class AbstractConfluencePluginWebTestCaseBase extends AbstractConfluencePluginWebTestCase {
    static final String TEST_SPACE_KEY = "ds";
    String remoteBaseUrl;
    WhitelistTestRule whitelistTestRule;
    CtkServerResourceModifier ctkServerResourceModifier;

    private static final Logger logger = LoggerFactory.getLogger(AbstractConfluencePluginWebTestCaseBase.class);

    @Override
    protected JWebUnitConfluenceWebTester createConfluenceWebTester() {
        try {
            Properties props = new Properties();
            props.setProperty("confluence.webapp.port", System.getProperty("http.port", "1990"));
            props.setProperty("confluence.webapp.context.path", System.getProperty("context.path", "/confluence"));
            return new JWebUnitConfluenceWebTester(new TesterConfiguration(props));
        } catch (IOException ioe) {
            fail("Unable to create tester: " + ioe.getMessage());
            return null;
        }
    }

    @Override
    public void flushCaches() {
        try {
            assertTrue(gotoPageWithEscalatedPrivileges("/admin/console.action"));
            clickLinkWithText("Cache Management");
            clickLinkWithText("Flush all");
        } finally {
            dropEscalatedPrivileges();
        }
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();

        ConfluenceWebTester confluenceWebTester = getConfluenceWebTester();
        String confluenceBaseUrl = confluenceWebTester.getBaseUrl();
        remoteBaseUrl = StringUtils.stripEnd(System.getProperty("baseurl.ctk-server", "http://localhost:8990"), "/");
        whitelistTestRule = WhitelistTestRule.withDefaultAdminLoginAndBaseUrl(confluenceBaseUrl);
        ctkServerResourceModifier = new CtkServerResourceModifier(remoteBaseUrl);
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();

        try {
            whitelistTestRule.tearDown();
        } catch (RuntimeException ex) {
            // This is needed because the whitelistTestRules do not properly handlethe new XSRF changes added in 5.9.
        }

        ctkServerResourceModifier.tearDown();
    }

    long createPage(String title, String content) {
        PageHelper helper = getPageHelper();

        helper.setSpaceKey(TEST_SPACE_KEY);
        helper.setParentId(0L);
        helper.setTitle(title);
        helper.setContent(content);
        helper.setCreationDate(new Date());
        helper.setLabels(emptyList());
        assertTrue(helper.create());

        // return the generated id for the new page
        return helper.getId();
    }

    /**
     * Views the page for a Page or BlogPost (AbstractPage)
     *
     * @param entityId the ID of the AbstractPage
     */
    void viewPageById(long entityId) {
        gotoPage("/pages/viewpage.action?pageId=" + entityId);
    }

    @Override
    public String getElementTextByXPath(String xpath) {
        try {
            return super.getElementTextByXPath(xpath);
        } catch (AssertionFailedError e) {
            logger.warn(tester.getPageSource());
            throw e;
        }
    }

    @Override
    public void assertElementNotPresentByXPath(String xpath) {
        try {
            super.assertElementNotPresentByXPath(xpath);
        } catch (AssertionFailedError e) {
            logger.warn(tester.getPageSource());
            throw e;
        }
    }

    @Override
    public void assertElementPresentByXPath(String xpath) {
        try {
            super.assertElementPresentByXPath(xpath);
        } catch (AssertionFailedError e) {
            logger.warn(tester.getPageSource());
            throw e;
        }
    }
}
