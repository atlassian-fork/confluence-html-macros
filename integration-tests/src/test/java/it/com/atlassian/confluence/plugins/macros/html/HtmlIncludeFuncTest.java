package it.com.atlassian.confluence.plugins.macros.html;

import com.atlassian.confluence.plugin.functest.ConfluenceWebTester;

public class HtmlIncludeFuncTest extends AbstractConfluencePluginWebTestCaseBase {
    private static final String MACRO_NAME = "html-include";
    private static final String REMOTE_RESOURCE = "/content.html";
    private long testPageId;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        enableHtmlIncludeMacro();
        ctkServerResourceModifier.setResourceFromFile(REMOTE_RESOURCE, "/com/atlassian/confluence/plugins/macros/html/content.html", "text/html");
        testPageId = createPage("HtmlIncludeFuncTest", createPageContent());
    }

    public void testWhitelistDenyWithoutAnyRule() {
        assertRemoteContentBlocked();
    }

    public void testWhitelistAllowUsingWildcard() {
        assertRemoteContentBlocked();
        whitelistTestRule.whitelistWildcard(remoteBaseUrl + "/*");
        assertRemoteContentIncluded();
    }

    public void testWhitelistAllowUsingExactMatch() {
        assertRemoteContentBlocked();
        whitelistTestRule.whitelistExactUrl(remoteBaseUrl + REMOTE_RESOURCE);
        assertRemoteContentIncluded();
    }

    public void testWhitelistAllowUsingRegex() {
        assertRemoteContentBlocked();
        whitelistTestRule.whitelistRegularExpression(remoteBaseUrl + "/[\\w]+.html");
        assertRemoteContentIncluded();
    }

    private void assertRemoteContentIncluded() {
        viewPageById(testPageId);
        assertTestPageShown();
    }

    private void assertRemoteContentBlocked() {
        viewPageById(testPageId);
        assertHtmlPageNotShown();
    }

    private void enableHtmlIncludeMacro() {
        String pluginKey = "confluence.macros.html-key";
        ConfluenceWebTester confluenceWebTester = getConfluenceWebTester();
        confluenceWebTester.enablePluginModule(pluginKey, "html-include-key", true);
        confluenceWebTester.enablePluginModule(pluginKey, "html-include-xhtml-key", true);
    }

    private void assertHtmlPageNotShown() {
        assertEquals("Could not access the content at the URL because it is not from an allowed source.",
                getElementTextByXPath("//div[@class='wiki-content']//p[1]//strong[1]"));
        assertEquals("Configure whitelist >>", getElementTextByXPath("//div[@class='wiki-content']//p[3]//a[1]"));
    }

    private void assertTestPageShown() {
        assertElementPresent("content-successful-embedded");
    }

    private String createPageContent() {
        return "{" + MACRO_NAME + ":url=" + remoteBaseUrl + REMOTE_RESOURCE + "}";
    }
}
